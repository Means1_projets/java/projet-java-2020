package fr.projetjava.common;

import lombok.Getter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class CollisionManager {

    @Getter
    private static boolean[][] result;

    /**
     * Vérifie si les coordonnées de l'entité est comprise dans une zone accessible
     *
     * @param x
     * @param y
     * @param w
     * @param h
     * @return
     */
    public static boolean checkPosition(int x, int y, int w, int h) {
        if (result == null)
            load();
        if (x < 0 || x > result.length || y < 0 || y > result[0].length || x + w > result.length || y + h > result[0].length) {
            return false;
        }
        for (int i = x; i <= x + w; i++) {
            if (!result[i][y] || !result[i][y + h])
                return false;
        }
        for (int i = y; i <= y + h; i++) {
            if (!result[x][i] || !result[x + w][i])
                return false;
        }
        return true;
    }

    /**
     * Charge la map
     */
    private static void load() {
        try {
            URL resource = CollisionManager.class.getResource("/collisionUpdate.png");

            BufferedImage image = ImageIO.read(resource);
            result = new boolean[image.getWidth()][image.getHeight()];
            for (int i = 0; i < result.length; i++) {
                for (int i1 = 0; i1 < result[i].length; i1++) {
                    result[i][i1] = (image.getRGB(i, i1) == -1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void init() {
        checkPosition(0, 0, 0, 0);
    }
}
