package fr.projetjava.common;

import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.Packet;
import fr.projetjava.common.network.packet.SpawnBulletPacket;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.FactoryPacketArme;
import fr.projetjava.common.utils.MouvementDirection;
import lombok.Getter;

import java.util.stream.Stream;

public enum ArmeType {

    SIMPLE(4, 50, EntityType.BULLET_SIMPLE,
            (type, id, x, y, angle) ->
                    new SpawnBulletPacket(new SpawnBulletPacket.SpawnBullet[]{
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle))))
                    }), 1,125, 0
    ),
    DOUBLE(4, 50, EntityType.BULLET_SIMPLE,
            (type, id, x, y, angle) ->
                    new SpawnBulletPacket(new SpawnBulletPacket.SpawnBullet[]{
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(-Math.cos(angle) * 5 + x,
                                            -Math.sin(angle) * 5 + y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(Math.cos(angle) * 5 + x,
                                            Math.sin(angle) * 5  + y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle))))
                    }), 1, 125, 1
    ),TRIPLE(4, 50, EntityType.BULLET_SIMPLE,
            (type, id, x, y, angle) ->
                    new SpawnBulletPacket(new SpawnBulletPacket.SpawnBullet[]{
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(-Math.cos(angle) * 10+ x,
                                            -Math.sin(angle) * 10 + y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(Math.cos(angle) * 10 + x,
                                            Math.sin(angle) * 10  + y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle))))
                    }), 1, 125, 2
    ),POMPE(4, 25, EntityType.BULLET_SIMPLE,
            (type, id, x, y, angle) ->
                    new SpawnBulletPacket(new SpawnBulletPacket.SpawnBullet[]{
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle-0.1), Math.sin(angle-0.1)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle+0.1), Math.sin(angle+0.1)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle-0.2), Math.sin(angle-0.2)))),
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle+0.2), Math.sin(angle+0.2)))),

                    }), 1, 500, 3
    ),FIREBALL(1, 100, EntityType.BULLET_FIREBALL,
            (type, id, x, y, angle) ->
                    new SpawnBulletPacket(new SpawnBulletPacket.SpawnBullet[]{
                            new SpawnBulletPacket.SpawnBullet(
                                    id,
                                    id,
                                    type,
                                    new MouvementDirection(x,
                                            y,
                                            0,
                                            new Direction(Math.cos(angle), Math.sin(angle))))

                    }), 10, 500,4
    ),;

    @Getter
    private int damage;
    @Getter
    private int id;
    @Getter
    private int speed;
    @Getter
    private int life;
    @Getter
    private EntityType bulletType;
    @Getter
    private int time;

    FactoryPacketArme runnable;

    ArmeType(int speed, int life, EntityType bulletType, FactoryPacketArme runnable, int damage, int time, int id) {
        this.speed = speed;
        this.life = life;
        this.time = time;
        this.id = id;
        this.damage = damage;
        this.bulletType = bulletType;
        this.runnable = runnable;
    }

    public Packet process(int id, double x, double y, double angle) {
        return this.runnable.process(this, id, x, y, angle);
    }

    public static ArmeType getById(int id){
        return Stream.of(values()).filter(p -> p.getId() == id).findFirst().orElse(null);
    }
}
