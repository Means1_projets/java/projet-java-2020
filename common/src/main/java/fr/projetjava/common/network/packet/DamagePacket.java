package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

/**
 * Appéllé quand il y a des dégats sur une entité
 */
public class DamagePacket extends Packet {

    private int id;
    private int target;
    private int damage;
}
