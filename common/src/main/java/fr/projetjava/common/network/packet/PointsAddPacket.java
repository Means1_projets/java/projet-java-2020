package fr.projetjava.common.network.packet;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
/**
 * Appellé quand un joueur gagne des points
 */
public class PointsAddPacket  extends Packet {
    private int id, points;
}
