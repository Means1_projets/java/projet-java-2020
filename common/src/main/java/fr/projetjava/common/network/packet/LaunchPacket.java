package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

/**
 * Appellé pour afficher le cooldown
 */
public class LaunchPacket extends Packet {

    private int time;

}
