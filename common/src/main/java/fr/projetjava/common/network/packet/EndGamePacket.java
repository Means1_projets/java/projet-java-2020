package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

/**
 * Appellé lors de la fin de partie
 */
public class EndGamePacket extends Packet {
    private boolean kick;
}
