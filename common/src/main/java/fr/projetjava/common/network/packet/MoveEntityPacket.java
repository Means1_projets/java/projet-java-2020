package fr.projetjava.common.network.packet;

import fr.projetjava.common.utils.MouvementDirection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
/**
 * Appellé quand une entité bouge
 */
public class MoveEntityPacket extends Packet {
    private int id;
    private MouvementDirection mouvementDirection;

}
