package fr.projetjava.common.network.packet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

/**
 * Appellé quand le client dit de faire avancer ou d'arrêter le vip
 */
public class ControllerBartPacket extends Packet {
    private int id;
}
