package fr.projetjava.common.entity;


import lombok.Getter;

@Getter
public enum EntityType {

    PLAYER("player"), MAP("map"), VIP("vip"), ZOMBIE("zombie"), BULLET_SIMPLE("bullet_simple"),BULLET_FIREBALL("bullet_fireball"), DROP("drop");

    private String name;

    EntityType(String name){
        this.name = name;
    }
}
