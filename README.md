# Projet Java 2020

Projet Java avec JavaFX : 

Jeu sur une map 2D, le but est d'escorter un personnage dans des batiments, il faut battre les zombies qui apparaissent aléatoirement sur la map
Il faut durer le plus longtemps

C'est un jeu multijoueur avec un server et des clients, il est donc possible de jouer à plusieurs avec des ordinateurs différents

Les zombies laisse des bonus, des nouvelles armes, de la vie...

## Images

![prez](img/prez.png)
![prez](img/prez2.png)

Requirement :

    Java 11+
    Gradle 6+
    JavaFX - https://gluonhq.com/products/javafx/

Gradle doit être en version 6+ et java en version 11+


Pour ne pas avoir d'erreur visuel sur intellij, il faut installer le plugin Lombok


Installation de JavaFX :
    Télécharger javafx depuis le lien
    Il faut mettre le contenu du zip de javafx dans les bons dossiers :
    le dossier lib du zip doit être dans client/lib (à créer si besoin)
    le dossier bin du zip (si présent) doit être dans client/bin (à créer si besoin)


Build le client :
1.      cd client
1.      gradle build

Lancer le client :
1.     cd client
1.     java --module-path ./lib --add-modules javafx.controls,javafx.fxml,javafx.swing,javafx.media -jar build/libs/client-1.0-SNAPSHOT.jar


Build le serveur :
1.     cd server
1.     gradle build

Lancer le server :
1.     cd server
1.     java -jar build/libs/server-1.0-SNAPSHOT.jar



Troubleshooting :

    Gradle, mauvaise version : 
        Pour windows, gradle doit avoir comme java_home le java11+ :
            - SET JAVA_HOME=path\to\jdk

    Erreur dans gradle : 
    
    le dossier client/lib (ou bin) ne contient pas javafx
    
    ou
    
    mauvaise version de gradle
    gradle --version
    
    ou
     
    mauvaise version de java
    
    java --version 
