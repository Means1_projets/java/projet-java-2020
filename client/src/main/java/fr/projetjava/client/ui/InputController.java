package fr.projetjava.client.ui;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.input.Input;
import com.almasb.fxgl.input.UserAction;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.entity.living.vip.VipClientEntity;
import fr.projetjava.client.entity.other.armes.DropClientEntity;
import fr.projetjava.common.Drop;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.ControllerBartPacket;
import fr.projetjava.common.network.packet.GetDropPacket;
import fr.projetjava.common.network.packet.HealEntityPacket;
import fr.projetjava.common.network.packet.SwitchArmePacket;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;


public class InputController {

    /**
     * Initialisation des inputs
     */
    public static void initInput() {
        Input input = FXGL.getInput();

        //Quand on appuye sur D
        input.addAction(new UserAction("Right") {
            @Override
            protected void onActionBegin() {
                Main.getInstance().getMouvementManager().beginRight();
            }

            @Override
            protected void onActionEnd() {
                Main.getInstance().getMouvementManager().endRight();
            }
        }, KeyCode.D);

        input.addAction(new UserAction("Up") {
            @Override
            protected void onActionBegin() {
                Main.getInstance().getMouvementManager().beginUp();
            }

            @Override
            protected void onActionEnd() {
                Main.getInstance().getMouvementManager().endUp();
            }
        }, KeyCode.Z);

        input.addAction(new UserAction("Left") {
            @Override
            protected void onActionBegin() {
                Main.getInstance().getMouvementManager().beginLeft();
            }

            @Override
            protected void onActionEnd() {
                Main.getInstance().getMouvementManager().endLeft();
            }
        }, KeyCode.Q);

        input.addAction(new UserAction("Bottom") {
            @Override
            protected void onActionBegin() {
                Main.getInstance().getMouvementManager().beginBottom();
            }

            @Override
            protected void onActionEnd() {
                Main.getInstance().getMouvementManager().endBottom();
            }
        }, KeyCode.S);


        //Quand on appuye sur E
        input.addAction(new UserAction("Interact") {
            @Override
            protected void onActionBegin(){
                PlayerClientEntity playerClientEntity = Main.getInstance().getMainEntity();
                //On récupère l'entity principale
                if (playerClientEntity != null) {
                    ClientEntity clientEntity = Main.getInstance().getEntities().stream().
                            filter(p -> p.getType().equals(EntityType.DROP) && p.getId() != -1).
                            filter(e -> e.getEntityGraphique().isColliding(playerClientEntity.getEntityGraphique()))
                            .findFirst().orElse(null);

                    //On récupère l'entité en collision avec l'entité principale
                    if (clientEntity instanceof DropClientEntity) {
                        if (((DropClientEntity) clientEntity).getDrop().isArme())
                            //Si c'est une arme, on change d'arme
                            Main.getInstance().getServer().getClientConnexion().send(new SwitchArmePacket(playerClientEntity.getId(), clientEntity.getId(), playerClientEntity.getArmeType()));
                        else
                            //Sinon on récupère dans l'inventaire
                            Main.getInstance().getServer().getClientConnexion().send(new GetDropPacket(playerClientEntity.getId(), clientEntity.getId(), ((DropClientEntity) clientEntity).getDrop()));
                    }
                }
            }
        }, KeyCode.E);

        //On appuye sur Space
        input.addAction(new UserAction("Space") {
            @Override
            protected void onActionBegin(){
                ClientEntity entity = Main.getInstance().getEntities().stream().filter(p -> p instanceof VipClientEntity).findFirst().orElse(null);
                if (entity == null)
                    return;
                //On envoie au serveur que bart avance ou s'arrête
                ControllerBartPacket packet = new ControllerBartPacket(entity.getId());
                Main.getInstance().getServer().getClientConnexion().send(packet);
            }
        }, KeyCode.SPACE);

        //On clique avec la souris
        input.addAction(new UserAction("click") {
            @Override
            protected void onActionBegin() {
                double mouseXWorld = input.getMouseXUI();
                double mouseYWorld = input.getMouseYUI();
                PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
                if (mainEntity != null && mouseXWorld >= 0 && mouseXWorld <= 127 && mouseYWorld >= FXGL.getAppHeight() - 230 && mouseYWorld <= FXGL.getAppHeight() - 142) {
                    int x = (int) ((mouseXWorld) / 29);
                    int y = (int) ((mouseYWorld - FXGL.getAppHeight() + 230) / 29);
                    int current = x + y * 4;
                    if (mainEntity.getInventory().size() <= current)
                        return;
                    Drop drop = mainEntity.getInventory().get(current);
                    //Partie interface graphique, on récupère l'item dans l'inventaire
                    switch (drop) {
                        case BIGMAC:
                        case KFC:
                        case KEBAB:
                            if (mainEntity.getCurrentLife() == mainEntity.getMaxLife())
                                return;
                            Main.getInstance().getServer().getClientConnexion().send(new HealEntityPacket(mainEntity.getId(),
                                    Math.min(mainEntity.getCurrentLife() + 10, mainEntity.getMaxLife())));
                            break;
                        case XP:
                            break;
                        case FORCE:
                            mainEntity.setDamage(mainEntity.getDamage()+1);
                            break;
                        case SPEED:
                            mainEntity.getMouvementDirection().setSpeed(mainEntity.getMouvementDirection().getSpeed() + 0.1);
                            break;
                    }
                    mainEntity.removeInventory(current);
                } else
                    Main.getInstance().getMouvementManager().beginMouse();
            }

            @Override
            protected void onActionEnd() {
                Main.getInstance().getMouvementManager().endMouse();
            }
        }, MouseButton.PRIMARY);
    }
}
