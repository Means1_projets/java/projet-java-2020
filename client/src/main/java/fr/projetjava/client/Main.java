package fr.projetjava.client;

import com.almasb.fxgl.app.ApplicationMode;
import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.SceneFactory;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.physics.CollisionHandler;
import com.almasb.fxgl.texture.Texture;
import com.almasb.fxgl.ui.FontFactory;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.living.ennemi.EnnemiClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.entity.other.armes.BulletClientEntity;
import fr.projetjava.client.manager.LaunchManager;
import fr.projetjava.client.manager.MouvementManager;
import fr.projetjava.client.manager.SpectatorManager;
import fr.projetjava.client.network.Server;
import fr.projetjava.client.network.listeners.PacketListener;
import fr.projetjava.client.ui.GameEntityFactory;
import fr.projetjava.client.ui.InputController;
import fr.projetjava.client.ui.menu.MainMenu;
import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.PacketManager;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Map;

public class Main extends GameApplication {

    @Getter
    public static Main instance;
    @Getter
    private final MouvementManager mouvementManager;

    @Getter
    private final ArrayList<ClientEntity> entities = new ArrayList<>();

    @Getter
    @Setter
    private int id = -1;

    @Setter
    @Getter
    private Server server;

    @Getter
    private final PacketManager packetManager;

    @Getter
    private final LaunchManager launchManager;
    @Getter
    private SpectatorManager spectatorManager;
    private Rectangle rectangle;


    /**
     * Fonction qui initialise la class principale du programme
     */
    public Main() {
        instance = this;
        //Initialise les manager.
        this.packetManager = new PacketManager();
        this.packetManager.registerListener(new PacketListener());
        this.launchManager = new LaunchManager();
        this.mouvementManager = new MouvementManager();
    }

    /**
     * Fonction principale
     *
     * @param args
     */
    public static void main(String[] args) {
        CollisionManager.init();
        launch(args);
        System.exit(0);
    }

    /**
     * Initialisation des settings
     *
     * @param settings
     */
    @Override
    protected void initSettings(GameSettings settings) {
        settings.setWidth(1000);
        settings.setHeight(1000);
        settings.setManualResizeEnabled(true);
        settings.setTitle("Java Escort Game");
        settings.setVersion("1");
        settings.setMenuEnabled(true);
        settings.setApplicationMode(ApplicationMode.DEVELOPER);
        settings.setSceneFactory(new SceneFactory() {
            @NotNull
            @Override
            public FXGLMenu newMainMenu() {
                return new MainMenu();
            }
        });
    }

    /**
     * Initialisation des inputs
     */
    @Override
    protected void initInput() {
        InputController.initInput();
    }

    /**
     * Initialisation des variables FXGL
     *
     * @param vars
     */
    @Override
    protected void initGameVars(Map<String, Object> vars) {
        vars.put("points", 0);
    }

    /**
     * Initialisation des collisions
     */
    @Override
    protected void initPhysics() {
        FXGL.getPhysicsWorld().setGravity(0, 0);
        FXGL.getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.BULLET_SIMPLE, EntityType.ZOMBIE) {
            @Override
            protected void onCollision(Entity a, Entity b) {
                if (a.isActive())
                    BulletClientEntity.process(a, b);
            }
        });
        FXGL.getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.BULLET_FIREBALL, EntityType.ZOMBIE) {
            @Override
            protected void onCollision(Entity a, Entity b) {
                if (a.isActive())
                    BulletClientEntity.process(a, b);
            }
        });
        FXGL.getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.ZOMBIE, EntityType.PLAYER) {
            @Override
            protected void onCollision(Entity a, Entity b) {
                EnnemiClientEntity.process(a, b);
            }
        });
        FXGL.getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.ZOMBIE, EntityType.VIP) {
            @Override
            protected void onCollision(Entity a, Entity b) {
                EnnemiClientEntity.process(a, b);
            }
        });
    }

    /**
     * Initialisation du jeu
     */
    @Override
    protected void initGame() {
        //Création du gestionnaire des entity fxgl
        FXGL.getGameWorld().addEntityFactory(new GameEntityFactory());
        FXGL.getGameScene().setCursor(FXGL.getAssetLoader().loadCursorImage("red_cursor.png"), new Point2D(0, 0));
        this.spectatorManager = new SpectatorManager();
        //Musiques
        //FXGL.getAudioPlayer().stopMusic(FXGL.getAssetLoader().loadMusic("ambiance.mp3"));
        //FXGL.getAudioPlayer().loopMusic(FXGL.getAssetLoader().loadMusic("ambience3.mp3"));
    }

    /**
     * Initialisation de l'interface graphique
     */
    @Override
    protected void initUI() {
        FontFactory stocky = FXGL.getAssetLoader().loadFont("stocky.ttf");
        FXGL.getGameScene().addUINode(launchManager.initText());
        Texture texture = FXGL.getAssetLoader().loadTexture("gunarea.png");
        texture.setTranslateX(10);
        texture.setTranslateY(FXGL.getAppHeight() - 100);
        FXGL.getGameScene().addUINode(texture);
        Texture texture2 = FXGL.getAssetLoader().loadTexture("inventory.png");
        texture2.setTranslateX(10);
        texture2.setTranslateY(FXGL.getAppHeight() - 200);
        FXGL.getGameScene().addUINode(texture2);
        Text textPixels = new Text();
        textPixels.setTranslateX(100);
        textPixels.setTranslateY(50);
        textPixels.setFill(Color.ORANGE);
        textPixels.textProperty().bind(FXGL.getGameState().intProperty("points").asString());
        Text points = new Text();
        points.setTranslateX(10);
        points.setTranslateY(50);
        points.setText("Points :");
        points.setFill(Color.ORANGE);
        Font font = stocky.newFont(20);
        points.setFont(font);
        textPixels.setFont(font);


        FXGL.getGameScene().addUINode(points);
        FXGL.getGameScene().addUINode(textPixels);


        Stop[] stops = new Stop[] {
                new Stop(0, Color.GREEN),
                new Stop(1, Color.TURQUOISE)
        };
        LinearGradient linearGradient =
                new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        rectangle = new Rectangle(70, 105, 155, 18);
        rectangle.setFill(linearGradient);

        Texture texture3 = FXGL.getAssetLoader().loadTexture("life.png");
        texture3.setScaleX(0.5);
        texture3.setScaleY(0.5);
        texture3.setTranslateX(-100);
        texture3.setTranslateY(50);
        FXGL.getGameScene().addUINode(rectangle);
        FXGL.getGameScene().addUINode(texture3);
    }

    /**
     * Permet de récupérer l'entité principale
     *
     * @return PlayerClientEntity
     */
    public PlayerClientEntity getMainEntity() {
        ClientEntity clientEntityById = getEntityById(getId());
        if (clientEntityById == null)
            return null;
        return (PlayerClientEntity) clientEntityById;
    }

    /**
     * Permet de récupérer l'entité en fonction de son id
     *
     * @param id
     * @return ClientEntity
     */
    public ClientEntity getEntityById(int id) {
        return new ArrayList<>(entities).stream().filter(p -> p.getId() == id).findAny().orElse(null);
    }

    /**
     * Lance le serveur en solo
     */
    public void startSolo() {
        fr.projetjava.server.Main.main("127.0.0.1");
        Main.getInstance().setServer(Server.connect("127.0.0.1", 25566));
    }

    /**
     * Lance le serveur en multi
     */
    public void startMulti() {
        Main.getInstance().setServer(Server.connect("45.76.32.192", 25566));
    }

    /**
     * Met à jour la vie du joueur au niveau de l'interface graphique
     */
    public void updateLife() {
        PlayerClientEntity mainEntity = getMainEntity();
        if (mainEntity != null)
            rectangle.setWidth(155 * ((double) mainEntity.getCurrentLife() / (double) mainEntity.getMaxLife()));
    }
}