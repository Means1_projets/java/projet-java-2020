package fr.projetjava.client.manager;

import com.almasb.fxgl.dsl.FXGL;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.CanMove;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.client.entity.other.armes.BulletClientEntity;
import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.network.packet.MoveEntityPacket;
import fr.projetjava.common.network.packet.Packet;
import fr.projetjava.common.utils.Direction;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class MouvementManager {

    private boolean click = false;
    private long time = System.currentTimeMillis();

    public MouvementManager() {
        run();
    }

    /**
     * Thread qui gère les mouvements
     */
    public void run() {
        try {
            new Thread(() -> {
                try {
                    while (true) {
                        try {
                            Thread.sleep(10);
                            if (Main.getInstance().getServer() != null) {
                                //Pour chaque entité
                                for (ClientEntity clientEntity : new ArrayList<>(Main.getInstance().getEntities())) {
                                    //Si elle peut bouger
                                    if (clientEntity instanceof CanMove) {
                                        if (clientEntity instanceof BulletClientEntity) {
                                            //On vérifie le temps de vie des bullets
                                            int life = ((BulletClientEntity) clientEntity).getLife();
                                            if (--life <= 0) {
                                                clientEntity.delete();
                                                break;
                                            }
                                            ((BulletClientEntity) clientEntity).setLife(life);
                                        }
                                        //On update sa position
                                        Platform.runLater(() -> {
                                            updatePos(clientEntity);
                                        });
                                    }
                                    //Action au click (pour tirer en continue)
                                    if (clientEntity.getId() == Main.getInstance().getId())
                                        if (clientEntity instanceof PlayerClientEntity && click && System.currentTimeMillis() - time > ((PlayerClientEntity) clientEntity).getArmeType().getTime()) {
                                            time = System.currentTimeMillis();
                                            PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
                                            double x = mainEntity.getEntityGraphique().getX() + 32;
                                            double y = mainEntity.getEntityGraphique().getY() + 32;
                                            double angle = -Math.atan2(FXGL.getInput().getMouseYWorld() - y, FXGL.getInput().getMouseXWorld() - x);
                                            if (angle < 0) {
                                                angle = 2 * Math.PI + angle;
                                            }
                                            angle = -angle + Math.PI / 2;
                                            Packet packet = mainEntity.getArmeType().process(mainEntity.getId(), x, y, angle);
                                            Main.getInstance().getServer().getClientConnexion().send(packet);
                                        }
                                }
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Throwable e){
                    e.printStackTrace();
                }
            }).start();
        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    private void updatePos(ClientEntity entity) {
        if (!entity.getEntityGraphique().isActive())
            return;
        Direction direction = entity.getMouvementDirection().getDirection();
        double x = entity.getOldPos().getX();
        double y = entity.getOldPos().getY();
        double tx = entity.getMouvementDirection().getX();
        double ty = entity.getMouvementDirection().getY();
        //S'il y a besoin d'une correction
        if ( Math.abs(x - tx) > 0.1 || Math.abs(y - ty) > 0.1){
            Correction find = null;
            if (entity.getCorrection() != null && entity.getCorrection().x == x - tx && entity.getCorrection().y == y - ty){
                find = entity.getCorrection();
            }
            if (find == null)
            {
                find = new Correction(entity.getId(), 50, x - tx, y - ty);
                entity.setCorrection(find);
            }

            double value = Math.sqrt(Math.pow(direction.getNorth(), 2)+Math.pow(direction.getRight(), 2));
            if (value != 0) {
                if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld() + (direction.getRight()/value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld() - (direction.getNorth()/value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())){
                    entity.getEntityGraphique().translate(
                            (direction.getRight()/value) * 2 * entity.getMouvementDirection().getSpeed() + -(find.getX())/50,
                            - (direction.getNorth()/value) * 2 * entity.getMouvementDirection().getSpeed() + -(find.getY())/50);
                } else if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld()),
                        (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld() - (direction.getNorth()/value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())){
                    entity.getEntityGraphique().translate(
                            -(find.getX())/50,
                            - (direction.getNorth()/value) * 2 * entity.getMouvementDirection().getSpeed() + -(find.getY())/50);
                } else if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld() + (direction.getRight()/value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld()),
                        (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())){
                    entity.getEntityGraphique().translate(
                            (direction.getRight()/value) * 2 * entity.getMouvementDirection().getSpeed() + -(find.getX())/50,
                            -(find.getY())/50);
                } else {
                    entity.getEntityGraphique().translate(
                            0 + -(find.getX()) / 50,
                            0 + -(find.getY()) / 50);

                }
            } else {
                entity.getEntityGraphique().translate(
                        0 + -(find.getX()) / 50,
                        0 + -(find.getY()) / 50);

            }
            find.setTick(find.getTick()-1);
            if (find.getTick() <= 0){
                entity.setCorrection(null);
                entity.setOldPos(new Point2D(entity.getMouvementDirection().getX(), entity.getMouvementDirection().getY()));
            }
        } else {
            double value = Math.sqrt(Math.pow(direction.getNorth(), 2) + Math.pow(direction.getRight(), 2));
            if (value != 0) {
                if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld() + (direction.getRight() / value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld() - (direction.getNorth() / value) * 2 * entity.getMouvementDirection().getSpeed()),
                        (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())) {


                    entity.getEntityGraphique().translate(
                            (direction.getRight() / value) * 2 * entity.getMouvementDirection().getSpeed(),
                            -(direction.getNorth() / value) * 2 * entity.getMouvementDirection().getSpeed());


                } else if (!(entity instanceof BulletClientEntity)){
                    if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld()),
                            (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld() - (direction.getNorth() / value) * 2 * entity.getMouvementDirection().getSpeed()),
                            (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())) {


                        entity.getEntityGraphique().translate(
                                0,
                                -(direction.getNorth() / value) * 2 * entity.getMouvementDirection().getSpeed());
                    } else if (CollisionManager.checkPosition((int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinXWorld() + (direction.getRight() / value) * 2 * entity.getMouvementDirection().getSpeed()),
                            (int) (entity.getEntityGraphique().getBoundingBoxComponent().getMinYWorld()),
                            (int) entity.getEntityGraphique().getBoundingBoxComponent().getWidth(), (int) entity.getEntityGraphique().getBoundingBoxComponent().getHeight())) {


                        entity.getEntityGraphique().translate(
                                (direction.getRight() / value) * 2 * entity.getMouvementDirection().getSpeed(),
                                0);
                    }
                }
            }
        }
    }

    /**
     * Notifier le serveur du mouvement
     */
    private void notifyServer() {
        MoveEntityPacket moveEntityPacket = new MoveEntityPacket();
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();

        if (!mainEntity.getMouvementDirection().getDirection().isNul())
            mainEntity.setLastDirection(mainEntity.getMouvementDirection().getDirection());
        mainEntity.getMouvementDirection().setX(mainEntity.getEntityGraphique().getX());
        mainEntity.getMouvementDirection().setY(mainEntity.getEntityGraphique().getY());


        moveEntityPacket.setId(Main.getInstance().getId());
        moveEntityPacket.setMouvementDirection(mainEntity.getMouvementDirection());

        mainEntity.setOldPos(new Point2D(mainEntity.getMouvementDirection().getX(), mainEntity.getMouvementDirection().getY()));


        Main.getInstance().getServer().getClientConnexion().send(moveEntityPacket);
    }

    public void beginRight() {
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(mainEntity.getMouvementDirection().getDirection().getNorth(), 1));
        updatePos(mainEntity);
        notifyServer();
    }

    public void endRight() {
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        if (mainEntity.getMouvementDirection().getDirection().getRight() != 1)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(mainEntity.getMouvementDirection().getDirection().getNorth(), 0));
        updatePos(mainEntity);
        notifyServer();
    }
    public void beginUp() {
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(1, mainEntity.getMouvementDirection().getDirection().getRight()));
        updatePos(mainEntity);
        notifyServer();
    }

    public void endUp() {
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        if (mainEntity.getMouvementDirection().getDirection().getNorth() != 1)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(0, mainEntity.getMouvementDirection().getDirection().getRight()));
        updatePos(mainEntity);
        notifyServer();
    }

    public void beginBottom(){
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(-1, mainEntity.getMouvementDirection().getDirection().getRight()));
        updatePos(mainEntity);
        notifyServer();
    }
    public void endBottom(){
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        if (mainEntity.getMouvementDirection().getDirection().getNorth() != -1)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(0, mainEntity.getMouvementDirection().getDirection().getRight()));
        updatePos(mainEntity);
        notifyServer();
    }

    public void beginLeft(){

        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(mainEntity.getMouvementDirection().getDirection().getNorth(), -1));
        updatePos(mainEntity);
        notifyServer();
    }

    public void endLeft(){
        PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
        if (mainEntity == null)
            return;
        if (mainEntity.getMouvementDirection().getDirection().getRight() != -1)
            return;
        mainEntity.getMouvementDirection().setDirection(new Direction(mainEntity.getMouvementDirection().getDirection().getNorth(), 0));
        updatePos(mainEntity);
        notifyServer();
    }

    public void beginMouse() {
        click = true;
    }

    public void endMouse() {
        click = false;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Correction {

        private int id, tick;
        private double x, y;
    }
}
