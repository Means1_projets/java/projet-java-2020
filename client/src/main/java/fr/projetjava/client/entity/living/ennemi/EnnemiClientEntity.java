package fr.projetjava.client.entity.living.ennemi;

import com.almasb.fxgl.entity.Entity;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.CanMove;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.component.EnnemiComponent;
import fr.projetjava.client.entity.component.EntityComponent;
import fr.projetjava.client.entity.living.LivingClientEntity;
import fr.projetjava.client.entity.other.armes.DropClientEntity;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.DamagePacket;
import lombok.Getter;

import java.util.HashMap;

public class EnnemiClientEntity extends LivingClientEntity implements CanMove {

    @Getter
    private final HashMap<ClientEntity, Long> livingClientEntityLongHashMap = new HashMap<>();

    /**
     * Création de l'entité ennemi
     *
     * @param id    l'id
     * @param x     le x
     * @param y     le y
     * @param level le level
     */
    public EnnemiClientEntity(int id, double x, double y, int level) {
        super(EntityType.ZOMBIE, id, x, y, (int) Math.pow(2, level));
        getEntityGraphique().getComponent(EnnemiComponent.class).setEnnemiClientEntity(this);
        setDamage(level);
    }

    /**
     * Permet de faire marche l'introduction avec le vip qui tire
     *
     * @param a
     * @param b
     */
    public static void process(Entity a, Entity b) {

        EntityComponent component = a.getComponent(EntityComponent.class);
        if (component != null && component.getClientEntity() instanceof EnnemiClientEntity && component.getClientEntity().getId() != -1) {
            EntityComponent component2 = b.getComponent(EntityComponent.class);
            if (component2 != null && component2.getClientEntity() instanceof LivingClientEntity) {
                HashMap<ClientEntity, Long> livingClientEntityLongHashMap = ((EnnemiClientEntity) component.getClientEntity()).getLivingClientEntityLongHashMap();
                if (!livingClientEntityLongHashMap.containsKey(component2.getClientEntity()) || livingClientEntityLongHashMap.containsKey(component2.getClientEntity()) && livingClientEntityLongHashMap.get(component2.getClientEntity()) + 1000 < System.currentTimeMillis()) {
                    livingClientEntityLongHashMap.put(component2.getClientEntity(), System.currentTimeMillis());
                    Main.getInstance().getServer().getClientConnexion().send(new DamagePacket(component.getClientEntity().getId(), component2.getClientEntity().getId(), ((EnnemiClientEntity) component.getClientEntity()).getDamage()));
                }
            }
        }
    }

    /**
     * Si c'est l'entité "intro" (avec l'id -1), on fait spawn un faux drop au sol
     */
    @Override
    public void delete() {
        if (getId() == -1) {
            ClientEntity entity = new DropClientEntity(-1, getEntityGraphique().getX(), getEntityGraphique().getY(), 0);
            new Thread(() -> {
                try {
                    Thread.sleep(5000);
                    entity.delete();
                } catch (InterruptedException e) {
                }
            }).start();
        }
        super.delete();
    }
}
