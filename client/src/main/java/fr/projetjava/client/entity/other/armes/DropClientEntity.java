package fr.projetjava.client.entity.other.armes;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.common.Drop;
import fr.projetjava.common.entity.EntityType;
import javafx.scene.image.ImageView;
import lombok.Getter;

public class DropClientEntity extends ClientEntity {

    @Getter
    Drop drop;


    /**
     * Création de l'entité au sol
     *
     * @param id   l'id
     * @param x    le x
     * @param y    le y
     * @param data l'id du type
     */
    public DropClientEntity(int id, double x, double y, int data) {
        super(EntityType.DROP, id, x, y);
        this.drop = Drop.getById(data);
        ImageView imageView = new ImageView(FXGL.getAssetLoader().loadImage("drop/" + drop.name().toLowerCase() + ".png"));
        getEntityGraphique().getViewComponent().addChild(imageView);
        getEntityGraphique().getBoundingBoxComponent().addHitBox(new HitBox(BoundingShape.box(imageView.getImage().getWidth(), imageView.getImage().getHeight())));
        getEntityGraphique().setScaleX(0.5);
        getEntityGraphique().setScaleY(0.5);
    }
}
