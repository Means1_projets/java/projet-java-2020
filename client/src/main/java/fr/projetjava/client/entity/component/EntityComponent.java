package fr.projetjava.client.entity.component;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.texture.Texture;
import fr.projetjava.client.entity.ClientEntity;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.Setter;

public class EntityComponent extends Component {

    @Getter
    @Setter
    private ClientEntity clientEntity;

    private long time;
    private Entity entity;


    /**
     * Création d'une bulle de tchat a côté
     *
     * @param message
     */
    public void setBulle(String message) {
        remove();
        time = System.currentTimeMillis();

        entity = FXGL.spawn("bulle", getEntity().getBoundingBoxComponent().getMaxXWorld() - 100, getEntity().getBoundingBoxComponent().getMinYWorld() - 200);
        Texture texture = FXGL.getAssetLoader().loadTexture("bulle.png");
        texture.setScaleX(0.75);
        texture.setScaleY(0.75);
        Text text = new Text();
        text.setText(message);
        text.setTranslateX(50);
        text.setTranslateY(70);
        entity.getViewComponent().addChild(texture);
        entity.getViewComponent().addChild(text);
    }

    /**
     * A chaque update
     *
     * @param tpf
     */
    @Override
    public void onUpdate(double tpf) {
        if (entity != null && entity.isActive()) {
            entity.setPosition(getEntity().getBoundingBoxComponent().getMaxXWorld() - 100, getEntity().getBoundingBoxComponent().getMinYWorld() - 200);
        }
        if (time + 10000 < System.currentTimeMillis()) {
            remove();
        }
    }

    /**
     * On remove la bulle de tchat
     */
    private void remove() {
        if (entity != null && entity.isActive()) {
            FXGL.getGameWorld().removeEntity(entity);
            entity = null;
        }
    }
}
