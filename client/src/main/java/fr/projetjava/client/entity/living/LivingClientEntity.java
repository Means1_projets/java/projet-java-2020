package fr.projetjava.client.entity.living;

import fr.projetjava.client.Main;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.component.LivingComponent;
import fr.projetjava.common.entity.EntityType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LivingClientEntity extends ClientEntity {

    private int currentLife;
    private int maxLife;
    private int damage = 1;

    /**
     * Création d'une entité avec de la vie
     *
     * @param type    le type
     * @param id      l'id
     * @param maxLife le max de vie
     */
    public LivingClientEntity(EntityType type, int id, int maxLife) {
        this(type, id, 0, 0, maxLife);
    }

    /**
     * Création d'une entité avec de la vie
     *
     * @param type    le type
     * @param id      l'id
     * @param x       le x
     * @param y       le y
     * @param maxLife la vie max
     */
    public LivingClientEntity(EntityType type, int id, double x, double y, int maxLife) {
        super(type, id, x, y);
        this.maxLife = maxLife;
        this.currentLife = maxLife;
        getEntityGraphique().getComponent(LivingComponent.class).setLivingClientEntity(this);

    }

    /**
     * appellé quand il y a des damages
     *
     * @param damage
     */
    public void damage(int damage) {
        currentLife -= damage;
        if (currentLife <= 0) {
            currentLife = 0;
            delete();
        }
        if (getId() == Main.getInstance().getId()) {
            Main.getInstance().updateLife();
        }
    }
}