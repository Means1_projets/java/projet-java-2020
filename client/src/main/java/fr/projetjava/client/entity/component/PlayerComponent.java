package fr.projetjava.client.entity.component;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.texture.AnimatedTexture;
import com.almasb.fxgl.texture.AnimationChannel;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import javafx.util.Duration;
import lombok.Setter;

public class PlayerComponent extends Component {

    private final AnimatedTexture texture;
    private final AnimationChannel animIdleRight;
    private final AnimationChannel animIdleLeft;
    private final AnimationChannel animIdleUp;
    private final AnimationChannel animIdleDown;
    private final AnimationChannel animWalkRight;
    private final AnimationChannel animWalkLeft;
    private final AnimationChannel animWalkUp;
    private final AnimationChannel animWalkDown;

    @Setter
    private PlayerClientEntity playerClientEntity;

    /**
     * Création du player component
     */
    public PlayerComponent() {
        animIdleUp = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 0, 0);
        animIdleRight = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 27, 27);
        animIdleDown = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 18, 18);
        animIdleLeft = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 9, 9);
        animWalkUp = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 0, 8);
        animWalkRight = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 27, 35);
        animWalkDown = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 18, 26);
        animWalkLeft = new AnimationChannel(FXGL.getAssetLoader().loadImage("personnage.png"), 9, 64, 64, Duration.seconds(1), 9, 17);

        texture = new AnimatedTexture(animIdleDown);
        texture.loop();


    }

    /**
     * On applique la texture lors de la création de l'entité
     */
    @Override
    public void onAdded() {
        entity.getViewComponent().addChild(texture);
    }

    /**
     * à chaque update
     *
     * @param tpf
     */
    @Override
    public void onUpdate(double tpf) {
        if (playerClientEntity == null)
            return;
        if (isMoving()) {
            if (playerClientEntity.getLastDirection().getRight() == 1) {
                if (texture.getAnimationChannel() != animWalkRight) {
                    texture.loopAnimationChannel(animWalkRight);
                }
            } else if (playerClientEntity.getLastDirection().getRight() == -1) {
                if (texture.getAnimationChannel() != animWalkLeft) {
                    texture.loopAnimationChannel(animWalkLeft);
                }
            } else {
                if (playerClientEntity.getLastDirection().getNorth() > 0){
                    if (texture.getAnimationChannel() != animWalkUp) {
                        texture.loopAnimationChannel(animWalkUp);
                    }
                } else {
                    if (texture.getAnimationChannel() != animWalkDown) {
                        texture.loopAnimationChannel(animWalkDown);
                    }
                }
            }
        } else {
            if (playerClientEntity.getLastDirection().getRight() == 1){
                if (texture.getAnimationChannel() != animIdleRight) {
                    texture.loopAnimationChannel(animIdleRight);
                }
            } else if (playerClientEntity.getLastDirection().getRight() == -1){
                if (texture.getAnimationChannel() != animIdleLeft) {
                    texture.loopAnimationChannel(animIdleLeft);
                }
            } else {
                if (playerClientEntity.getLastDirection().getNorth() > 0){
                    if (texture.getAnimationChannel() != animIdleUp) {
                        texture.loopAnimationChannel(animIdleUp);
                    }
                } else {
                    if (texture.getAnimationChannel() != animIdleDown) {
                        texture.loopAnimationChannel(animIdleDown);
                    }
                }
            }
        }
    }

    /**
     * Retourne vrai si l'entité bouge
     *
     * @return un boolean
     */
    public boolean isMoving() {
        return !playerClientEntity.getMouvementDirection().getDirection().isNul() || playerClientEntity.getCorrection() != null;
    }



}
