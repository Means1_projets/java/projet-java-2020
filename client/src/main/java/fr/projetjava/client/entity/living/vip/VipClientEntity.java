package fr.projetjava.client.entity.living.vip;

import fr.projetjava.client.entity.CanMove;
import fr.projetjava.client.entity.component.VipComponent;
import fr.projetjava.client.entity.living.LivingClientEntity;
import fr.projetjava.common.entity.EntityType;

public class VipClientEntity extends LivingClientEntity implements CanMove {
    /**
     * Création du vip
     *
     * @param id l'id
     * @param x  le x
     * @param y  le y
     */
    public VipClientEntity(int id, double x, double y) {
        super(EntityType.VIP, id, x, y, 100);
        getEntityGraphique().getComponent(VipComponent.class).setVipClientEntity(this);
        getMouvementDirection().setSpeed(0.25);
    }
}

