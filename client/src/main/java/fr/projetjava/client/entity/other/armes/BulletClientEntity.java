package fr.projetjava.client.entity.other.armes;

import com.almasb.fxgl.entity.Entity;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.CanMove;
import fr.projetjava.client.entity.ClientEntity;
import fr.projetjava.client.entity.component.EntityComponent;
import fr.projetjava.client.entity.living.LivingClientEntity;
import fr.projetjava.client.entity.living.player.PlayerClientEntity;
import fr.projetjava.common.ArmeType;
import fr.projetjava.common.network.packet.DamagePacket;
import fr.projetjava.common.utils.MouvementDirection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulletClientEntity extends ClientEntity implements CanMove {
    private ArmeType armeType;
    private int life;
    private int owner;
    private boolean hit = false;

    /**
     * L'entité "bullet"
     *
     * @param id                 son id
     * @param armeType           le type d'arme
     * @param mouvementDirection Sa direction
     * @param owner              l'id de celui qui tire
     */
    public BulletClientEntity(int id, ArmeType armeType, MouvementDirection mouvementDirection, int owner) {
        super(armeType.getBulletType(), id, mouvementDirection.getX(), mouvementDirection.getY());
        setMouvementDirection(mouvementDirection);
        this.armeType = armeType;
        this.life = this.armeType.getLife();
        this.owner = owner;
    }

    /**
     * Appellé quand une entité touche l'autre
     *
     * @param a
     * @param b
     */
    public static void process(Entity a, Entity b) {
        ClientEntity entity = a.getComponent(EntityComponent.class).getClientEntity();
        if (!(entity instanceof BulletClientEntity) || ((BulletClientEntity) entity).isHit())
            return;
        ((BulletClientEntity) entity).setHit(true);
        if (((BulletClientEntity) entity).getOwner() == Main.getInstance().getId()) {
            LivingClientEntity clientEntity = (LivingClientEntity) b.getComponent(EntityComponent.class).getClientEntity();
            PlayerClientEntity mainEntity = Main.getInstance().getMainEntity();
            if (mainEntity == null)
                return;
            Main.getInstance().getServer().getClientConnexion().send(
                    new DamagePacket(((BulletClientEntity) entity).getOwner(), clientEntity.getId(), ((BulletClientEntity) entity).getArmeType().getDamage()*mainEntity.getDamage()));
        } else if (entity.getId() == -1 && b.getComponent(EntityComponent.class).getClientEntity().getId() == -1){
            ((LivingClientEntity) b.getComponent(EntityComponent.class).getClientEntity()).damage(1);
        }
        entity.delete();
    }
}
