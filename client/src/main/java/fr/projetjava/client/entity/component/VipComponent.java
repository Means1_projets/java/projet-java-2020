package fr.projetjava.client.entity.component;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.texture.AnimatedTexture;
import com.almasb.fxgl.texture.AnimationChannel;
import fr.projetjava.client.entity.living.vip.VipClientEntity;
import javafx.util.Duration;
import lombok.Setter;

public class VipComponent extends Component {

    private final AnimatedTexture texture;
    private final AnimationChannel animIdleRight;
    private final AnimationChannel animIdleLeft;
    private final AnimationChannel animIdleUp;
    private final AnimationChannel animIdleDown;
    private final AnimationChannel animWalkRight;
    private final AnimationChannel animWalkLeft;
    private final AnimationChannel animWalkUp;
    private final AnimationChannel animWalkDown;

    @Setter
    private VipClientEntity vipClientEntity;


    /**
     * Création du vip component qui gère l'animation
     */
    public VipComponent() {
        animIdleUp = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(1), 10, 10);
        animIdleRight = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(1), 7, 7);
        animIdleDown = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(1), 1, 1);
        animIdleLeft = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(1), 4, 4);
        animWalkUp = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(0.25), 9, 11);
        animWalkRight = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(0.25), 6, 8);
        animWalkDown = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(0.25), 0, 2);
        animWalkLeft = new AnimationChannel(FXGL.getAssetLoader().loadImage("bartSprite.png"), 3, 48, 48, Duration.seconds(0.25), 3, 5);

        texture = new AnimatedTexture(animIdleDown);
        texture.loop();

    }

    /**
     * Lors de la création, on défini la texture
     */
    @Override
    public void onAdded() {
        entity.getViewComponent().addChild(texture);
    }

    /**
     * A chaque fois que l'on update l'écran
     *
     * @param tpf
     */
    @Override
    public void onUpdate(double tpf) {
        if (vipClientEntity == null)
            return;
        if (isMoving()) {
            double i = Math.abs(vipClientEntity.getLastDirection().getRight());
            double j = Math.abs(vipClientEntity.getLastDirection().getNorth());
            if (i > j) {
                if (vipClientEntity.getLastDirection().getRight() > 0) {
                    if (texture.getAnimationChannel() != animWalkRight) {
                        texture.loopAnimationChannel(animWalkRight);
                    }
                } else if (vipClientEntity.getLastDirection().getRight() < 0){
                    if (texture.getAnimationChannel() != animWalkLeft) {
                        texture.loopAnimationChannel(animWalkLeft);
                    }
                }
            } else {
                if (vipClientEntity.getLastDirection().getNorth() > 0){
                    if (texture.getAnimationChannel() != animWalkUp) {
                        texture.loopAnimationChannel(animWalkUp);
                    }
                } else {
                    if (texture.getAnimationChannel() != animWalkDown) {
                        texture.loopAnimationChannel(animWalkDown);
                    }
                }
            }
        } else {
            double i = Math.abs(vipClientEntity.getLastDirection().getRight());
            double j = Math.abs(vipClientEntity.getLastDirection().getNorth());
            if (i > j){
                if (vipClientEntity.getLastDirection().getRight() > 0){
                    if (texture.getAnimationChannel() != animIdleRight) {
                        texture.loopAnimationChannel(animIdleRight);
                    }
                } else if (vipClientEntity.getLastDirection().getRight() < 0){
                    if (texture.getAnimationChannel() != animIdleLeft) {
                        texture.loopAnimationChannel(animIdleLeft);
                    }
                }
            } else {
                if (vipClientEntity.getLastDirection().getNorth() > 0){
                    if (texture.getAnimationChannel() != animIdleUp) {
                        texture.loopAnimationChannel(animIdleUp);
                    }
                } else {
                    if (texture.getAnimationChannel() != animIdleDown) {
                        texture.loopAnimationChannel(animIdleDown);
                    }
                }
            }
        }
    }


    /**
     * Retourne vrai si l'entité bouge
     *
     * @return un boolean
     */
    public boolean isMoving() {
        return !vipClientEntity.getMouvementDirection().getDirection().isNul();
    }



}
