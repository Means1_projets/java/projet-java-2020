package fr.projetjava.client.entity.living.player;

import com.almasb.fxgl.app.scene.Viewport;
import com.almasb.fxgl.dsl.FXGL;
import fr.projetjava.client.Main;
import fr.projetjava.client.entity.CanMove;
import fr.projetjava.client.entity.component.PlayerComponent;
import fr.projetjava.client.entity.living.LivingClientEntity;
import fr.projetjava.common.ArmeType;
import fr.projetjava.common.Drop;
import fr.projetjava.common.entity.EntityType;
import javafx.application.Platform;
import javafx.scene.image.ImageView;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class PlayerClientEntity extends LivingClientEntity implements CanMove {

    @Getter
    @Setter
    private ArmeType armeType = ArmeType.SIMPLE;

    private ImageView armeNode;
    @Getter
    private final ArrayList<Drop> inventory = new ArrayList<>();
    @Getter
    private final ArrayList<ImageView> inventoryView = new ArrayList<>();


    /**
     * Création du player
     *
     * @param id l'id
     * @param x  le x
     * @param y  le y
     */
    public PlayerClientEntity(int id, double x, double y) {
        super(EntityType.PLAYER, id, x, y, 100);
        getEntityGraphique().getComponent(PlayerComponent.class).setPlayerClientEntity(this);
        if (id == Main.getInstance().getId()) {
            Viewport viewport = FXGL.getGameScene().getViewport();
            viewport.bindToEntity(getEntityGraphique(), FXGL.getAppHeight() / 2, FXGL.getAppWidth() / 2);

            viewport.setLazy(true);
            Main.getInstance().getSpectatorManager().leftSpectator();
            Main.getInstance().updateLife();
            updateArme();

        }
    }


    /**
     * Update l'image sur l'interface graphique
     */
    public void updateArme() {
        if (armeNode != null) {
            FXGL.getGameScene().removeUINode(armeNode);
        }
        armeNode = new ImageView(FXGL.getAssetLoader().loadImage("drop/arme_" + armeType.name().toLowerCase() + ".png"));

        armeNode.setTranslateX(60 - armeNode.getImage().getWidth() / 2);
        armeNode.setTranslateY(FXGL.getAppHeight() - 50 - armeNode.getImage().getHeight() / 2);
        FXGL.getGameScene().addUINode(armeNode);
    }

    /**
     * Ajoute un item dans l'inventaire
     *
     * @param drop le drop
     */
    public void addInventory(Drop drop) {
        inventory.add(drop);
        updateInv();
    }

    /**
     * Enleve un item de l'inventaire
     *
     * @param i le i
     */
    public void removeInventory(int i) {
        inventory.remove(i);
        updateInv();
    }

    /**
     * Update de l'inventaire
     */
    public void updateInv() {
        Platform.runLater(() -> {
            inventoryView.forEach(FXGL.getGameScene()::removeUINode);
            inventoryView.clear();
            int i = 0;
            for (Drop drop : inventory) {
                ImageView imageView = new ImageView(FXGL.getAssetLoader().loadImage("drop/" + drop.name().toLowerCase() + ".png"));
                imageView.setFitHeight(29);
                imageView.setFitWidth(29);
                imageView.setTranslateX(10 + (i % 4) * 29);
                imageView.setTranslateY(FXGL.getAppHeight() - 200 + (i / 4) * 29);
                inventoryView.add(imageView);
                FXGL.getGameScene().addUINode(imageView);
                i++;
            }
        });
    }

    /**
     * Si c'est l'entité principale on met le mode spectateur
     */
    @Override
    public void delete() {
        super.delete();
        if (Main.getInstance().getId() == getId() || Main.getInstance().getMainEntity() == null) {
            if (Main.getInstance().getId() == getId()) {
                Main.getInstance().getSpectatorManager().joinSpectator();
            }
            Main.getInstance().getEntities().stream().filter(p -> p instanceof PlayerClientEntity && ((PlayerClientEntity) p).getCurrentLife() > 0).findFirst().ifPresent(entity -> {
                FXGL.getGameScene().getViewport().unbind();
                FXGL.getGameScene().getViewport().bindToEntity(entity.getEntityGraphique(), FXGL.getAppHeight() / 2, FXGL.getAppWidth() / 2);
            });
        }
    }
}
