package fr.projetjava.client.network;

import fr.projetjava.client.Main;
import lombok.Getter;

@Getter
public class Server {


    private final ClientConnexion clientConnexion;
    private final String ip;
    private final int port;

    /**
     * Création de la connexion avec le serveur
     *
     * @param ip   l'ip du serveur
     * @param port le port du serveur
     */
    public Server(String ip, int port) {

        this.ip = ip;
        this.port = port;


        clientConnexion = new ClientConnexion(ip, port);
        Thread t = new Thread(clientConnexion);
        t.start();
    }

    /**
     * connection au serveur
     *
     * @param ip   ip
     * @param port port
     * @return Server le serveur
     */
    public static Server connect(String ip, int port) {
        return new Server(ip, port);
    }

    /**
     * Arrêt du serveur
     */
    public void stop() {
        if (clientConnexion != null)
            clientConnexion.stop();
        Main.getInstance().setServer(null);
    }
}
