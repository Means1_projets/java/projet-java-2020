package fr.projetjava.client.network;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import fr.projetjava.client.Main;
import fr.projetjava.common.network.packet.Packet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class ClientConnexion implements Runnable{

    private Socket sock = null;
    private PrintWriter writer = null;
    private BufferedInputStream reader = null;
    private String tmp = "";

    /**
     * Connexion au serveur !
     * @param host
     * @param port
     */
    public ClientConnexion(String host, int port) {
        try {
            sock = new Socket(host, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Boucle principale
     */
    public void run() {
        System.out.println("Lancement de la connexion !");
        try {
            while (!sock.isClosed()) {
                try {
                    read();
                } catch (UnrecognizedPropertyException | StringIndexOutOfBoundsException e) {

                } catch (SocketException e) {
                    System.err.println("connexion perdue ! ");
                    writer = null;
                    reader = null;
                    try {
                        sock.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Main.getInstance().setServer(null);

        } catch (Throwable e){
            e.printStackTrace();
        }
    }

    /**
     * Appellé lors de la lecture du contenu du buffer
     * @throws IOException
     */
    private void read() throws Exception{
        //Lecture du buffer
        reader = new BufferedInputStream(sock.getInputStream());
        int stream;
        byte[] b = new byte[4096];
        stream = reader.read(b);
        String response = tmp + new String(b, 0, stream);
        tmp = "";

        //On coupe la réponse avec le séparateur ";"
        //En effet chaque packet elle séparé par un ";"

        String[] packets = response.split(";");

        //Si le dernier méssage fini par ";", alors c'est la fin du message, sinon il manque une partie
        if (response.endsWith(";")){
            for (String packet : packets) {
                //Pour chaque packet, on créer l'objet associé et on notifie le client qu'il a recu un packet
                Packet convert = Main.getInstance().getPacketManager().convert(packet);
                Main.getInstance().getPacketManager().call(convert);
            }
        } else {
            for (int i = 0; i < packets.length - 1; i++) {
                Packet convert = Main.getInstance().getPacketManager().convert(packets[i]);
                Main.getInstance().getPacketManager().call(convert);
            }
            //On stock ce qui est le début du méssage dans une variable tmp
            tmp = packets[packets.length-1];
        }
    }

    /**
     * Permet d'envoyer un packet au serveur
     * @param packet
     */
    public void send(Packet packet){
        String convert = Main.getInstance().getPacketManager().convert(packet);
        if (convert != null) {
            try {
                send(convert);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Permet d'envoyer une chaine de caractères au serveur
     *
     * @param text chaine de caractères qui contient l'info
     * @throws IOException
     */
    private void send(String text) throws IOException {
        writer = new PrintWriter(sock.getOutputStream());
        //On rajoute le fameux ";"
        writer.write(text+";");
        writer.flush();
    }

    public void stop() {
        try {
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}