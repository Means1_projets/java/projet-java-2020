package fr.projetjava.server.entity.living;

import fr.projetjava.common.entity.EntityType;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.game.Game;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Getter
@Setter
public class LivingServerEntity extends ServerEntity {

    private int currentLife;
    private int maxLife;

    /**
     * Création d'une entité avec de la vie
     *
     * @param type        le type
     * @param game        la partie
     * @param rectangle   la hitbox
     * @param currentLife la vie
     */
    public LivingServerEntity(EntityType type, Game game, Rectangle rectangle, int currentLife) {
        super(type, game, rectangle);
        this.currentLife = currentLife;
        this.maxLife = currentLife;
    }
}
