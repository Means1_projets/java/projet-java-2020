package fr.projetjava.server.entity;

public interface TimeLife {

    int getTime();

    void decrement();
}
