package fr.projetjava.server.entity.living;

import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.MoveEntityPacket;
import fr.projetjava.common.network.packet.Packet;
import fr.projetjava.common.network.packet.SpawnEntityPacket;
import fr.projetjava.common.utils.Direction;
import fr.projetjava.common.utils.MouvementDirection;
import fr.projetjava.server.entity.CanMove;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.Target;
import fr.projetjava.server.game.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

public class EnnemiServerEntity extends LivingServerEntity implements CanMove {
    private final int level;
    private Direction direction = new Direction();
    private final long spawnTime = System.currentTimeMillis();

    /**
     * Création d'un enemy
     *
     * @param game  la partie
     * @param level le niveau
     */
    public EnnemiServerEntity(Game game, int level) {
        super(EntityType.ZOMBIE, game, new Rectangle(10, 0, 40, 62), (int) Math.pow(2, level));
        this.level = level;
    }

    /**
     * Création du packet pour le spawn
     *
     * @return
     */
    @Override
    public SpawnEntityPacket.SpawnEntity getPacket() {
        return new SpawnEntityPacket.SpawnEntity(getId(), getX(), getY(), getType(), level);
    }


    /**
     * A chaque update on défini les mouvements et s'il y a des changements, on averti les joueurs
     */
    public void processMouvement() {
        if (System.currentTimeMillis() < spawnTime + 2000)
            return;


        Optional<ServerEntity> first = new ArrayList<>(getGame().getEntities())
                .stream().filter(p -> p instanceof Target)
                .min(Comparator.comparingInt(e -> (int) (Math.pow(e.getX() - getX(), 2) + Math.pow(e.getY() - getY(), 2))));

        double value = Math.sqrt(Math.pow(direction.getNorth(), 2) + Math.pow(direction.getRight(), 2));
        if (value != 0) {
            if (CollisionManager.checkPosition((int) (getBbox().getX() + getX() + (direction.getRight() / value) * 2),
                    (int) (getBbox().getY() + getY() - (direction.getNorth() / value) * 2),
                    (int) getBbox().getWidth(), (int) getBbox().getHeight())) {
                setX(getX() + (direction.getRight() / value));
                setY(getY() - (direction.getNorth() / value));
            } else if (CollisionManager.checkPosition((int) (getBbox().getX() + getX()),
                    (int) (getBbox().getY() + getY() - (direction.getNorth() / value) * 2),
                    (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                setY(getY() - (direction.getNorth()/value));
            } else if (CollisionManager.checkPosition((int) (getBbox().getX()+getX() + (direction.getRight()/value) * 2),
                    (int) (getBbox().getY()+getY()),
                    (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                setX(getX() + (direction.getRight()/value));
            }
        }

        Direction direction;
        if (first.isPresent()) {
            //S'il y a une personne proche
            ServerEntity entity = first.get();
            if (Math.pow(entity.getX()- getX(), 2) + Math.pow(entity.getY() - getY(), 2) < 100000) {

                //Et que le zombie peut la voir, on défini sa direction à partir de ses coordonnées
                int x = (int) (getX() + getBbox().getX() + getBbox().getWidth() / 2 - (entity.getX() + entity.getBbox().getX() + entity.getBbox().getWidth() / 2));
                int y = (int) (getY() + getBbox().getY() + getBbox().getHeight() / 2 - (entity.getY() + entity.getBbox().getY() + entity.getBbox().getHeight() / 2));
                if (x > 2 * y && 2 * x < y) {
                    direction = new Direction(-1, 1);
                } else if (-x >= -2 * y && -2 * x < -y) {
                    direction = new Direction(1, -1);
                } else if (2 * x <= -y && x > -2 * y) {
                    direction = new Direction(1, 1);
                } else if (2 * x >= -y && x < -2 * y) {
                    direction = new Direction(-1, -1);
                } else if (2 * x >= y && x > -y * 2) {
                    direction = new Direction(0, -1);
                } else if (x <= -2 * y  && -x < -2 * y){
                    direction = new Direction(-1, 0);
                } else if (-2 * x >= -y && -2*x < y){
                    direction = new Direction(1, 0);
                } else {
                    direction = new Direction(0, 1);
                }

            } else
                direction = new Direction();
        } else
            direction = new Direction();


        //S'il y a un changement, on averti les joueurs
        if (!this.direction.equals(direction)){
            this.direction = direction;
            Packet packet = new MoveEntityPacket(getId(), new MouvementDirection(getX(), getY(), 0.5, this.direction));
            getGame().players.forEach(p -> p.getClientProcessor().send(packet));
        }
    }
}
