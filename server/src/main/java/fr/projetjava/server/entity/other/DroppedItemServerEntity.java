package fr.projetjava.server.entity.other;

import fr.projetjava.common.Drop;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.SpawnEntityPacket;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.TimeLife;
import fr.projetjava.server.game.Game;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Getter
@Setter
/**
 * Entité qui représente un drop au sol
 */
public class DroppedItemServerEntity extends ServerEntity implements TimeLife {
    private Drop drop;
    private int time = 2000;

    public DroppedItemServerEntity(EntityType type, Game game, Drop drop) {
        super(type, game, new Rectangle());
        this.drop = drop;
    }

    @Override
    public SpawnEntityPacket.SpawnEntity getPacket(){
        return new SpawnEntityPacket.SpawnEntity(getId(), getX(), getY(), getType(), drop.getId());
    }

    @Override
    public int getTime() {
        return this.time;
    }

    @Override
    public void decrement() {
        this.time--;
    }
}
