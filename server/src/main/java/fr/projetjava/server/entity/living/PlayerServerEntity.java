package fr.projetjava.server.entity.living;

import fr.projetjava.common.CollisionManager;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.utils.MouvementDirection;
import fr.projetjava.server.entity.CanMove;
import fr.projetjava.server.entity.Target;
import fr.projetjava.server.game.Game;
import fr.projetjava.server.network.ClientProcessor;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Getter
public class PlayerServerEntity extends LivingServerEntity implements Target, CanMove {

    private final ClientProcessor clientProcessor;
    @Setter
    private MouvementDirection mouvementDirection;

    /**
     * Création d'un player côté serveur avec une {@link ClientProcessor}
     *
     * @param clientProcessor la cônnection
     * @param game            la game
     */
    public PlayerServerEntity(ClientProcessor clientProcessor, Game game) {
        super(EntityType.PLAYER, game, new Rectangle(16, 16, 32, 48), 100);
        this.clientProcessor = clientProcessor;
    }

    /**
     * A chaque update, on défini les nouvelles coordonnées
     */
    @Override
    public void processMouvement() {
        if (mouvementDirection != null) {
            double value = Math.sqrt(Math.pow(mouvementDirection.getDirection().getNorth(), 2) + Math.pow(mouvementDirection.getDirection().getRight(), 2));
            if (value != 0) {
                if (CollisionManager.checkPosition((int) (getBbox().getX() + getX() + (mouvementDirection.getDirection().getRight() / value) * 2 * mouvementDirection.getSpeed()),
                        (int) (getBbox().getY() + getY() - (mouvementDirection.getDirection().getNorth() / value) * 2 * mouvementDirection.getSpeed()),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())) {
                    setX(getX() + (mouvementDirection.getDirection().getRight() / value) * 2 * mouvementDirection.getSpeed());
                    setY(getY() - (mouvementDirection.getDirection().getNorth() / value) * 2 * mouvementDirection.getSpeed());
                } else if (CollisionManager.checkPosition((int) (getBbox().getX()+getX()),
                        (int) (getBbox().getY()+getY() - (mouvementDirection.getDirection().getNorth()/value) * 2 * mouvementDirection.getSpeed()),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                    setY(getY() - (mouvementDirection.getDirection().getNorth()/value) * 2 * mouvementDirection.getSpeed());
                } else if (CollisionManager.checkPosition((int) (getBbox().getX()+getX() + (mouvementDirection.getDirection().getRight()/value) * 2 * mouvementDirection.getSpeed()),
                        (int) (getBbox().getY()+getY()),
                        (int) getBbox().getWidth(), (int) getBbox().getHeight())){
                    setY(getY() - (mouvementDirection.getDirection().getNorth()/value) * 2 * mouvementDirection.getSpeed());
                }
            }
        }
    }
}
