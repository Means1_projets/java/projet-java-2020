package fr.projetjava.server.entity;


import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.packet.SpawnEntityPacket;
import fr.projetjava.server.game.Game;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

public class ServerEntity {

    @Getter
    private final Game game;
    @Getter
    int id;
    @Getter
    @Setter
    private double x, y;

    @Getter
    private final Rectangle bbox;

    private static int idCount = 0;
    @Getter
    private final EntityType type;

    /**
     * Création d'une entité côté serveur
     *
     * @param type      le type
     * @param game      le game
     * @param rectangle la hitbox
     */
    public ServerEntity(EntityType type, Game game, Rectangle rectangle) {
        this.game = game;
        this.type = type;
        this.bbox = rectangle;
        this.id = ++idCount;
    }

    /**
     * Création du packet de spawn
     *
     * @return
     */
    public SpawnEntityPacket.SpawnEntity getPacket() {
        return new SpawnEntityPacket.SpawnEntity(getId(), getX(), getY(), getType(), 0);
    }
}
