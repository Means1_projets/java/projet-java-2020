package fr.projetjava.server.graph;

import java.util.Random;

public class TargetNode extends Node {
    private String[] strings;

    public TargetNode(String name, double x, double y, String[] strings) {
        super(name, x, y);
        this.strings = strings;
    }

    public String getRandomText() {
        return strings[new Random().nextInt(strings.length)];
    }
}
