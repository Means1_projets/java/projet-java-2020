package fr.projetjava.server.graph;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Node {
    private String name;
    private double x, y;
    private Double distance = Double.MAX_VALUE;
    public List<Node> shortestPath = new LinkedList<>();

    Map<Node, Double> adjacentNodes = new HashMap<>();

    public Node(String name, double x, double y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    /**
     * Ajoute une connection entre deux nodes avec la distance comme le poid de la conection
     *
     * @param destination node
     */
    public void addNextNode(Node destination) {
        double xB = destination.getX();
        double yB = destination.getY();
        double distance = Math.sqrt(Math.pow(xB - this.getX(), 2.0) + Math.pow(yB - this.getY(), 2.0));
        adjacentNodes.put(destination, distance);
        destination.adjacentNodes.put(this, distance);
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", distance=" + distance +
                '}';
    }
}
