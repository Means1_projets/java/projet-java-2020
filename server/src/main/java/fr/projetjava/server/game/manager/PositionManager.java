package fr.projetjava.server.game.manager;

import fr.projetjava.common.network.packet.Packet;
import fr.projetjava.common.network.packet.RemoveEntityPacket;
import fr.projetjava.server.entity.CanMove;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.TimeLife;
import fr.projetjava.server.game.Game;

import java.util.ArrayList;

public class PositionManager {

    private final Game game;

    public PositionManager(Game game) {
        this.game = game;
        run();
    }


    /**
     * Thread qui gère la position des entités
     */
    public void run() {
        new Thread(() -> {
            try {
                while (!game.isEndGame()) {
                    try {
                        ArrayList<ServerEntity> entities = new ArrayList<>();
                        new ArrayList<>(game.getEntities()).stream().filter(p -> p instanceof TimeLife).forEach(p -> {
                            ((TimeLife) p).decrement();
                            int time = ((TimeLife) p).getTime();
                            if (time <= 0) {
                                entities.add(p);
                            }
                        });
                        game.getEntities().removeAll(entities);
                        for (ServerEntity entity : entities) {
                            Packet packet = new RemoveEntityPacket(entity.getId());
                            game.getPlayers().forEach(p -> p.getClientProcessor().send(packet));
                        }

                        for (ServerEntity entity : new ArrayList<>(game.getEntities())) {
                            if (entity instanceof CanMove){
                                ((CanMove) entity).processMouvement();
                            }
                        }

                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            } catch (Throwable e){
                e.printStackTrace();
            }
        }).start();
    }
}
