package fr.projetjava.server.game.manager;

import fr.projetjava.common.network.packet.MessagePacket;
import fr.projetjava.common.network.packet.PointsAddPacket;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.living.PlayerServerEntity;
import fr.projetjava.server.entity.living.VipServerEntity;
import fr.projetjava.server.game.Game;
import fr.projetjava.server.graph.Graph;
import fr.projetjava.server.graph.Node;
import fr.projetjava.server.graph.TargetNode;
import lombok.Getter;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Getter
public class VipServerManager {


    Game game;
    String current;

    public VipServerManager(Game game) {
        this.game = game;
    }

    /**
     * Quand un vip arrive à destination
     */
    public void finishRound() {
        SpawnManager spawnManager = game.getSpawnManager();
        spawnManager.setLevel(spawnManager.getLevel() + 1);
        spawnManager.setCap(spawnManager.getCap() + 5);
        game.getPlayers().forEach(p -> p.getClientProcessor().send(new PointsAddPacket(p.getId(), 100)));
        processNext();
    }

    /**
     * Choix de la prochaine destinations
     */
    private void processNext() {
        ServerEntity serverEntity = game.getEntities().stream().filter(p -> p instanceof VipServerEntity).findFirst().orElse(null);
        if (serverEntity != null) {

            Node objectif = ((VipServerEntity) serverEntity).getObjectif();
            List<Node> voiture = Graph.graph.getNodes().stream().filter(p -> p instanceof TargetNode && !p.equals(objectif)).collect(Collectors.toList());
            TargetNode node = (TargetNode) voiture.get(new Random().nextInt(voiture.size()));
            MessagePacket packet = new MessagePacket(serverEntity.getId(), node.getRandomText());

            //Envoie aux joueurs de la bulle de tchat
            for (PlayerServerEntity player : game.players) {
                player.getClientProcessor().send(packet);
            }
            ((VipServerEntity) serverEntity).setObjectif(null);
            new Thread(() -> {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (objectif == null)
                    current = "Voiture";
                else
                    current = objectif.getName();

                ((VipServerEntity) serverEntity).setObjectif(Graph.calculeCheminLePlusCourt(current, node.getName()));
            }).start();
        }
    }

    public void start(){
        processNext();
    }
}
