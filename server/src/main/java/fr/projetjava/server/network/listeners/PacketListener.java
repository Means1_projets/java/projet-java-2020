package fr.projetjava.server.network.listeners;

import fr.projetjava.common.Drop;
import fr.projetjava.common.entity.EntityType;
import fr.projetjava.common.network.Listener;
import fr.projetjava.common.network.PacketHandler;
import fr.projetjava.common.network.packet.*;
import fr.projetjava.server.Main;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.living.LivingServerEntity;
import fr.projetjava.server.entity.living.PlayerServerEntity;
import fr.projetjava.server.entity.living.VipServerEntity;
import fr.projetjava.server.entity.other.BulletServerEntity;
import fr.projetjava.server.entity.other.DroppedItemServerEntity;
import fr.projetjava.server.game.Game;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class qui sera un listener qui contient les fonctions qui seront appellé automatiquement.
 */
public class PacketListener implements Listener {

    @PacketHandler
    public void onPacketMoveEntity(MoveEntityPacket packet){
        ServerEntity serverEntity = Main.getInstance().getEntityById(packet.getId());
        serverEntity.setX(packet.getMouvementDirection().getX());
        serverEntity.setY(packet.getMouvementDirection().getY());
        ((PlayerServerEntity) serverEntity).setMouvementDirection(packet.getMouvementDirection());
        Game game = serverEntity.getGame();
        if (game.isEndGame())
            return;
        for (PlayerServerEntity player : game.players) {
            if (player.getId() != packet.getId()){
                player.getClientProcessor().send(packet);
            }
        }

    }

    @PacketHandler
    public void onSwitchArmePacket(SwitchArmePacket packet){
        ServerEntity serverEntity = Main.getInstance().getEntityById(packet.getId());
        Game game = serverEntity.getGame();
        if (game.isEndGame())
            return;
        ServerEntity entity = game.getEntities().stream().filter(p -> p.getId() == packet.getIdArme()).findFirst().orElse(null);
        if (!(entity instanceof DroppedItemServerEntity))
            return;
        DroppedItemServerEntity droppedItemServerEntity = new DroppedItemServerEntity(EntityType.DROP, game, Drop.getByArme(packet.getType()));
        droppedItemServerEntity.setX(entity.getX());
        droppedItemServerEntity.setY(entity.getY());
        game.getEntities().add(droppedItemServerEntity);
        SpawnEntityPacket packet1 = new SpawnEntityPacket(new SpawnEntityPacket.SpawnEntity[]{droppedItemServerEntity.getPacket()});
        game.getEntities().remove(entity);
        RemoveEntityPacket packet2 = new RemoveEntityPacket(packet.getIdArme());
        for (PlayerServerEntity player : game.players) {
            player.getClientProcessor().send(packet1);
            player.getClientProcessor().send(packet2);
        }
        game.getPlayers().stream().filter(p -> p.getId() == packet.getId()).forEach(p -> p.getClientProcessor().send(new SwitchArmePacket(p.getId(), 0, ((DroppedItemServerEntity) entity).getDrop().getType())));
    }

    @PacketHandler
    public void onPacketSpawnBullet(SpawnBulletPacket packet){
        ServerEntity entity = Main.getInstance().getEntityById(packet.getSpawnBullets()[0].getId());
        Game game = entity.getGame();
        if (game.isEndGame())
            return;
        for (SpawnBulletPacket.SpawnBullet spawnBullet : packet.getSpawnBullets()) {
            BulletServerEntity bulletServerEntity = new BulletServerEntity(spawnBullet.getArmeType().getBulletType(), game, spawnBullet.getArmeType(), spawnBullet.getMouvementDirection());
            game.entities.add(bulletServerEntity);
            spawnBullet.setId(bulletServerEntity.getId());
        }
        game.players.forEach(p -> p.getClientProcessor().send(packet));
    }

    @PacketHandler
    public void onPacketDamage(DamagePacket packet){
        ServerEntity serverEntity = Main.getInstance().getEntityById(packet.getTarget());
        if (serverEntity == null)return;
        Game game = serverEntity.getGame();
        if (game.isEndGame())
            return;
        if (serverEntity instanceof LivingServerEntity){
            ((LivingServerEntity) serverEntity).setCurrentLife(((LivingServerEntity) serverEntity).getCurrentLife()-packet.getDamage());
            if (((LivingServerEntity) serverEntity).getCurrentLife() <= 0){
                ServerEntity user = Main.getInstance().getEntityById(packet.getId());
                if (user instanceof PlayerServerEntity){
                    ((PlayerServerEntity) user).getClientProcessor().send(new PointsAddPacket(packet.getId(), 1));
                }
                kill(game, serverEntity);
                if (game.getPlayers().stream().noneMatch(p -> p.getCurrentLife() > 0) || game.getEntities().stream().noneMatch(p -> p instanceof VipServerEntity)){
                    game.setEndGame(true);
                    game.getPlayers().forEach(p -> p.getClientProcessor().send(new EndGamePacket()));
                    new Thread(() -> {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                        }
                        game.getPlayers().forEach(p -> p.getClientProcessor().setForce(true));
                        game.getPlayers().forEach(p -> p.getClientProcessor().send(new EndGamePacket(true)));
                        game.stop();
                    }).start();
                }
            } else {
                game.getPlayers().forEach(p -> p.getClientProcessor().send(packet));
            }
        }
    }

    @PacketHandler
    public void onPacketGetDrop(GetDropPacket packet){
        ServerEntity serverEntity = Main.getInstance().getEntityById(packet.getItemId());
        if (serverEntity == null)return;
        Game game = serverEntity.getGame();
        if (game.isEndGame())
            return;
        new ArrayList<>(game.players).stream().filter(p -> p.getId() == packet.getId()).forEach(p -> p.getClientProcessor().send(packet));
        new ArrayList<>(game.players).forEach(p -> p.getClientProcessor().send(new RemoveEntityPacket(packet.getItemId())));

    }

    private void kill(Game game, ServerEntity serverEntity){
        game.getEntities().remove(serverEntity);
        if (new Random().nextInt(3) == 0){
            Drop drop = Drop.random();
            DroppedItemServerEntity droppedItemServerEntity = new DroppedItemServerEntity(EntityType.DROP, game, drop);
            droppedItemServerEntity.setX(serverEntity.getX());
            droppedItemServerEntity.setY(serverEntity.getY());
            game.getEntities().add(droppedItemServerEntity);
            SpawnEntityPacket packet1 = new SpawnEntityPacket(new SpawnEntityPacket.SpawnEntity[]{droppedItemServerEntity.getPacket()});
            game.players.forEach(p -> p.getClientProcessor().send(packet1));
        }
        game.players.forEach(p -> p.getClientProcessor().send(new RemoveEntityPacket(serverEntity.getId())));
    }

    @PacketHandler
    public void onHealPacket(HealEntityPacket packet){
        ServerEntity serverEntity = Main.getInstance().getEntityById(packet.getId());
        if (!(serverEntity instanceof LivingServerEntity))return;
        Game game = serverEntity.getGame();
        if (game.isEndGame())
            return;
        ((LivingServerEntity) serverEntity).setCurrentLife(packet.getLife());
        game.players.forEach(p -> p.getClientProcessor().send(packet));
    }

    @PacketHandler
    public void onControllerPacketBart(ControllerBartPacket packet){
        ServerEntity entityById = Main.getInstance().getEntityById(packet.getId());
        if (entityById != null){
            ((VipServerEntity) entityById).toggleStop();
        }
    }
}
