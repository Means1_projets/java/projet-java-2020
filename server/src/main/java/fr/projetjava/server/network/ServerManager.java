package fr.projetjava.server.network;

import fr.projetjava.server.Main;
import fr.projetjava.server.entity.living.PlayerServerEntity;
import fr.projetjava.server.game.Game;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerManager {


    private int port = 25566;
    private ServerSocket server = null;
    private final boolean isRunning = true;
    private Thread t;

    /**
     * Création du serveur
     */
    public ServerManager() {
        try {
            server = new ServerSocket(port, 100, InetAddress.getByName("127.0.0.1"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Création d'une serveur avec un ip et un port
     *
     * @param pHost
     * @param pPort
     */
    public ServerManager(String pHost, int pPort) {
        port = pPort;
        try {
            server = new ServerSocket(port, 100, InetAddress.getByName(pHost));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ouverture du port et écoute des clients
     */
    public void open() {
        //On écoute les clients qui veulent se connecté
        new Thread(() -> {
            try {
                boolean stop = false;
                while (!stop) {

                    try {
                        Socket client = server.accept();

                        System.out.println("new Client");

                        //On récupère la première game possible qui est ouverte
                        Game firstAvailableGame = Main.getInstance().getFirstAvailableGame();
                        ClientProcessor clientProcessor = new ClientProcessor(client);
                        PlayerServerEntity playerEntity = new PlayerServerEntity(clientProcessor, firstAvailableGame);

                        Thread t1 = new Thread(clientProcessor);
                        t1.start();
                        //On ajoute le joueur
                        new Thread(() -> {
                            firstAvailableGame.addPlayer(playerEntity);
                        }).start();



                        //On lance le traitement des infos du client (read/write)

                    } catch (Exception e) {
                        stop = true;
                    }
                }

            } catch (Throwable e){
                e.printStackTrace();
            }
        }).start();
    }

    public void close() {
        try {
            if (server != null)
                server.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
