package fr.projetjava.server;

import fr.projetjava.common.network.PacketManager;
import fr.projetjava.server.entity.ServerEntity;
import fr.projetjava.server.entity.living.PlayerServerEntity;
import fr.projetjava.server.game.Game;
import fr.projetjava.server.network.ClientProcessor;
import fr.projetjava.server.network.ServerManager;
import fr.projetjava.server.network.listeners.PacketListener;
import lombok.Getter;

import java.util.ArrayList;

public class Main {

    @Getter
    public static Main instance;

    @Getter
    private final PacketManager packetManager;
    @Getter
    private final ServerManager server;

    @Getter
    private final ArrayList<Game> games = new ArrayList<>();


    /**
     * création du serveur
     *
     * @param host l'ip
     */
    public Main(String host) {
        instance = this;
        packetManager = new PacketManager();
        packetManager.registerListener(new PacketListener());
        if (host != null)
            server = new ServerManager(host, 25566);
        else
            server = new ServerManager();
    }

    /**
     * fonction principale
     *
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main(null);
        main.getServer().open();
    }

    /**
     * fonction principale
     *
     * @param host l'ip
     */
    public static void main(String host) {
        Main main = new Main(host);
        main.getServer().open();
    }

    /**
     * Arret du serveur
     */
    public static void stop() {
        if (instance != null) {
            for (Game game : instance.getGames()) {
                game.players.forEach(p -> p.getClientProcessor().stop());
            }
            instance.getServer().close();
            instance = null;
        }
    }

    /**
     * Récupère la premiere partie non démarré
     *
     * @return
     */
    public Game getFirstAvailableGame() {
        return games.stream().filter(p -> !p.isRunnning()).findFirst().orElse(new Game());
    }

    /**
     * Récupère l'entité avec son id
     *
     * @param id son id
     * @return {@link ServerEntity}
     */
    public ServerEntity getEntityById(int id) {
        return games.stream().flatMap(p -> p.getEntities().stream()).filter(p -> p.getId() == id).findFirst().orElse(null);
    }

    /**
     * Récupère l'entité avec ClientProcessor
     *
     * @param clientProcessor sa connection
     * @return Le player
     */
    public PlayerServerEntity getEntityByClientPorcessor(ClientProcessor clientProcessor) {
        return games.stream().flatMap(p -> p.getPlayers().stream()).filter(p -> p.getClientProcessor() == clientProcessor).findFirst().orElse(null);
    }
}
